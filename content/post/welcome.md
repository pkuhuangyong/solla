---
title: "Welcome"
date: 2023-08-08T01:22:29+08:00
lastmod: 2023-08-08T01:22:29+08:00
draft: false
keywords: []
description: ""
tags: ["daily"]
categories: ["随记"]
author: "Solla"

# Uncomment to pin article to front page
# weight: 1
# You can also close(false) or open(true) something for this content.
# P.S. comment can only be closed
comment: false
toc: true
autoCollapseToc: false
# You can also define another contentCopyright. e.g. contentCopyright: "This is another copyright."
contentCopyright: "版权归诗人海子所有。"
reward: false
mathjax: false

# Uncomment to add to the homepage's dropdown menu; weight = order of article
# menu:
#   main:
#     parent: "docs"
#     weight: 1
---

欢迎来到我的博客，祝愿你过的幸福。

<!--more-->

# 送你一首幸福的诗

> 从明天起，做一个幸福的人
>
> 喂马、劈柴，周游世界
>
> 从明天起，关心粮食和蔬菜
>
> 我有一所房子，面朝大海，春暖花开
>
> 从明天起，和每一个亲人通信
>
> 告诉他们我的幸福
>
> 那幸福的闪电告诉我的
>
> 我将告诉每一个人
>
> 给每一条河每一座山取一个温暖的名字
>
> 陌生人，我也为你祝福
>
> 愿你有一个灿烂的前程
>
> 愿你有情人终成眷属
>
> 愿你在尘世获得幸福
>
> 我只愿面朝大海，春暖花开

海子 《面朝大海，春暖花开》
