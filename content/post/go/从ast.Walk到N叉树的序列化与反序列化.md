---
title: "从 ast.Walk 到 N 叉树的序列化/反序列化"
date: 2023-08-15T00:22:31+08:00
lastmod: 2023-08-15T00:22:31+08:00
draft: false
keywords: ["golang","LeetCode","LeetCode 428","N-ary Tree","序列化","反序列化","ast.Walk","Visitor Pattern","设计模式"]
description: ""
tags: ["golang","LeetCode","序列化与反序列化","设计模式","ast"]
categories: ["golang","LeetCode"]
author: "Solla"

# Uncomment to pin article to front page
# weight: 1
# You can also close(false) or open(true) something for this content.
# P.S. comment can only be closed
comment: false
toc: true
autoCollapseToc: false
# You can also define another contentCopyright. e.g. contentCopyright: "This is another copyright."
contentCopyright: "转载请注明出处。"
reward: false
mathjax: false

# Uncomment to add to the homepage's dropdown menu; weight = order of article
# menu:
#   main:
#     parent: "docs"
#     weight: 1
---

抽象语法树（Abstract Syntax Tree，AST）是编译过程中的一个中间产物，是源代码语法结构的一种抽象表示，它以树状的形式表现编程语言的语法结构，有了树结构，就可以对其进行遍历，达到代码检查、压缩、优化等目的。

Go 提供了几个官方用于 AST 分析的包
>
> - [go/scanner](https://pkg.go.dev/go/scanner)：词法解析，将源代码分割成一个个 token
> - [go/token](https://pkg.go.dev/go/token)：token 类型及相关结构体定义
> - [go/ast](https://pkg.go.dev/go/ast)：ast 的结构定义
> - [go/parser](https://pkg.go.dev/go/parser)：语法分析，读取 token 流生成 ast

其中 go/ast 包中的 Walk 函数用于 AST 的遍历。

<!--more-->

## 问题：一行多余的代码

最近在翻看以前的项目代码，有一个部分要对用户查询条件生成的 AST 进行遍历，同时检查节点类型及运算符是否合法，项目中的实现参考了 Go ast 包中 walk.go 里的写法，重新定义了 Visitor、Walk、Node 等要素，并在 Walk 的实现中完全参考了 ast.Walk 的写法：Walk 以深度优先顺序遍历 AST，对遍历到的节点调用 `v.Visit(node)`。如果 `v.Visit(node)` 返回的 Visitor 不是 nil，则对 node 的每个非 nil 子节点递归调用 Walk，最后调用 `w.Visit(nil)`。

```go {.line-numbers}
// Go 官方库 go/ast 包中的 Walk 实现

// Walk traverses an AST in depth-first order: It starts by calling
// v.Visit(node); node must not be nil. If the visitor w returned by
// v.Visit(node) is not nil, Walk is invoked recursively with visitor
// w for each of the non-nil children of node, followed by a call of
// w.Visit(nil).
//
func Walk(v Visitor, node Node) {
    if v = v.Visit(node); v == nil {
        return
    }

    // walk children
    // (the order of the cases matches the order
    // of the corresponding node types in ast.go)
    switch n := node.(type) {
    // Comments and fields
    case *Comment:
    // nothing to do

    // 中间有非常多的类型判断，此处省略

    case *Package:
        for _, f := range n.Files {
        Walk(v, f)
    }

    default:
        panic(fmt.Sprintf("ast.Walk: unexpected node type %T", n))
    }

    v.Visit(nil)    // 这一句就是困扰我的问题
}
```

可以看出 Walk 的实现逻辑很简单，包括 3 部分：

- 开头的 if 判断，决定是否继续下一级的遍历
- 中间的 `switch-type` 类型断言，处理不同类型的节点，递归地遍历子节点
- 最后的空节点访问 `v.Visit(nil)`

我项目中的实现几乎和官方实现一样，也是 3 个部分，只不过中间的类型断言是针对项目自身实现的节点类型做的。疑惑就出在 Walk 代码的最后一行 `w.Visit(nil)`，在项目 Visit 方法的实现里并没有对参数为 nil 作特殊处理，实际上 Visit 的参数为 nil 时，Visit 什么也不做，也就是说 Walk 的实现有这一行没有这一行其实没有区别，那么它为什么会出现在这里（我知道是抄的 ast.Walk，求求啦，别骂啦）？这个问题困惑了我好久。

查看 Go ast 包中 Visit 方法的实现（总共找到 6 个实现），几乎都没有对 Node 是 nil 的特殊处理，也没有发现这一行到底起到什么作用

|![](https://gitlab.com/pkuhuangyong/imgbed/-/raw/main/pictures/2023/08/12_17_2_8_202308121702987.png) |![](https://gitlab.com/pkuhuangyong/imgbed/-/raw/main/pictures/2023/08/12_16_58_43_202308121658991.png) |
---|---
![](https://gitlab.com/pkuhuangyong/imgbed/-/raw/main/pictures/2023/08/12_16_42_31_202308121642209.png) |![](https://gitlab.com/pkuhuangyong/imgbed/-/raw/main/pictures/2023/08/12_16_43_44_202308121643921.png) |
|![](https://gitlab.com/pkuhuangyong/imgbed/-/raw/main/pictures/2023/08/12_16_45_12_202308121645235.png) |![](https://gitlab.com/pkuhuangyong/imgbed/-/raw/main/pictures/2023/08/12_17_0_2_202308121659833.png) |

 GitHub 上很多第三方库里对自己 AST 的遍历实现也是参考了 Go 官方包中 ast.Walk 的写法，甚至注释都一模一样，依旧没有发现任何端倪。网上翻了好久资料都没有对 Walk 实现的解释，直到在知乎上看到一篇 [LeetCode 题解：一文带你看懂二叉树的序列化](https://zhuanlan.zhihu.com/p/164408048)，我似乎找到了解释 ast.Walk 实现的方向。

## 灵感：二叉树的序列化

>❝ 序列化（serialization）在计算机科学的数据处理中，是指将数据结构或对象状态转换成可取用格式（例如存成文件，存于缓冲，或经由网络中发送），以留待后续在相同或另一台计算机环境中，能恢复原先状态的过程。依照序列化格式重新获取字节的结果时，可以利用它来产生与原始对象相同语义的副本。对于许多对象，像是使用大量引用的复杂对象，这种序列化重建的过程并不容易。面向对象中的对象序列化，并不概括之前原始对象所关系的函数。这种过程也称为对象编组（marshalling）。从一系列字节提取数据结构的反向操作，是反序列化（也称为解编组、deserialization、unmarshalling）。❞

[一文带你看懂二叉树的序列化](https://zhuanlan.zhihu.com/p/164408048) 一文中在 DFS 序列化二叉树时，增加了对空节点的输出，当我看到这 `增加对空节点输出` 这几个字时，感觉好像和 ast.Walk 中最后一行 `w.Visit(nil)` 呼应上了。

我们在对二叉树进行普通遍历时，通常不会输出空节点，这种输出紧凑，只包含树中实际存在地节点信息，但是弊端就是无法通过输出结果唯一得重建一棵树（二叉树的唯一重建需要前序遍历结果和中序遍历结果或者后续遍历结果和中序遍历结果）。在这篇文章中，作者增加了对空节点的输出。通过空节点标记了一棵子树的结束，可以对通过这种方式序列化的二叉树进行反序列化（二叉树重建）。

AST 不就是一棵树吗？抽象语法树啊！在 Go 中它是否也有序列化/反序列化的需求？它的序列化/反序列化是不是和二叉树的序列化/反序列化类似？

于是顺着这个方向去研究 Go AST 的结构，发现 AST 中的节点被抽象为接口 Node，Go 编译过程中生成的 AST 由各种类型的实现了 Node 接口的其他类型（struct，func，pointer）组成。每种类型的 Node 的子节点又不尽相同，有的 Node 可以拥有多个子节点，而有的 Node 一个子节点都没有。也就是说 Go 中的 AST 在本质上是一棵 N 叉树，而且树中节点可能拥有不同的类型。

考虑 N 叉树的序列化，如果它需要输出空节点来标记子树的结束，那么它和二叉树的序列化有什么区别？

上面那篇文章中，作者对空节点的输出实际上分别输出了叶节点的左右空孩子，这是因为二叉树最多有两棵子树，所以可以通过 left，right 标记输出。但是 N 叉树节点最多可以有 N 棵子树，把 N 个空孩子都输出一遍肯定是很愚蠢的做法，那么换一种思路，对于 N 叉树中的每一个节点，我们认为它所拥有的子节点数量是它的非空子节点数量加 1，即我们认为每一个节点都有一个空节点作为最后一个孩子节点，这样当我们对当前节点的子节点进行遍历时，遍历到空孩子节点就知道当前节点的所有子节点已经遍历完毕，可以返回了。就像是下图一样：
![N 叉树遍历模型](https://gitlab.com/pkuhuangyong/imgbed/-/raw/main/pictures/2023/08/15_19_1_58_%E5%A4%9A%E5%8F%89%E6%A0%91%E9%81%8D%E5%8E%86%E6%A8%A1%E5%9E%8B.png)

我们写一下按照这种思路序列化 N 叉树的伪代码：

```go {.line-numbers}
traverse(node){
    // 首先序列化当前节点
    dosomething(node)

    // 递归地序列化当前节点的子节点
    for children of node{
        traverse(children)
    }

    // 序列化当前节点的最后一个节点，即我们想象中的空孩子节点
    dosomething(nil)
}

```

呼应上了！呼应上了！

这简直和 ast.Walk 一模一样！现在我有理由相信 ast.Walk 的实现是考虑了 AST 的序列化/反序列化需求，但是在高兴之前首先应该验证这种序列化方式是否能够工作，毕竟目前为止的所有分析都是根据二叉树序列化方法推出来的猜想。

OK，去 LeetCode 验证一下。恰巧第 428 到题目就是 N 叉树的序列化与反序列化。

在真正解题之前，我们先来梳理一下 ast.Walk 的实现的特点。

## 涨知识：ast.Walk 中的 Visitor 模式

### Visitor 模式

>
> 访问者模式是一种将算法与对象结构分离的软件设计模式。
> 这个模式的基本想法如下：
>
> - 首先我们拥有一个由许多对象构成的对象结构，这些对象的类都拥有一个 accept 方法用来接受访问者对象；
> - 访问者是一个接口，它拥有一个 visit 方法，这个方法对访问到的对象结构中不同类型的元素作出不同的反应；
> - 在对象结构的一次访问过程中，我们遍历整个对象结构，对每一个元素都实施 accept 方法，在每一个元素的 accept 方法中回调访问者的 visit 方法，从而使访问者得以处理对象结构的每一个元素。
>
>访问者模式的核心思想是解耦数据结构与数据操作，使得对元素的操作具备优秀的扩展性，我们可以通过扩展不同的数据操作类型(访问者)实现对相同元素的不同操作。

上面是 wikiwand 上对访问者模式的解释，看起来很抽象，不太好理解，确实是这样，有人说访问者模式是 23 种设计模式中最复杂的一种。下面尝试用大白话解释一下。

### 个人解读

我们把由许多对象元素构成的复杂对象结构（可能是树，集合，列表等）想象成一个拥有许多公司的产业园，那么各家公司就对应复杂对象结构中的各个对象元素；对象元素可能是不同类型，那么对应我们产业园的公司属于各个不同行业。产业园的各家公司有时候需要各种服务，比如外墙刷新、报告厅布置、招聘服务等等，这里的各种服务就是各种 Visitor 各自实现的不同的 Visit 方法，所以 Visitor 是提供各种服务的人才的总称，提供实际服务的就是 Visitor 的具体实现，比如提供外墙刷新服务的 Visitor 叫 Painter，提供报告厅布置服务的 Visitor 叫 Decorator，提供招聘服务的叫 Hunter 等等，它们在各自的 Visit 方法实现中提供各自的服务。

当一个公司需要外墙刷新服务时，只需要实现一个 Painter，让 Painter 去访问该公司，对该公司使用 Visit 方法（实现了刷新外墙的服务）即可

现在有了产业园——复杂数据结构、公司——对象元素、技术人才——各种 Visitor、服务——数据操作，那么还缺少一种能找到服务需求公司的方法，那就是复杂数据结构元素的遍历方法，不同的数据结构遍历的方案不同，对于列表、集合等容器型数据结构，遍历可以通过其迭代器实现，对于树、图等结构，遍历方式有 BFS 和 DFS 等。

### ast.Walk 中 Visitor 模式的应用

#### 访问者 Visitor

Go 中 ast.Walk.go 使用 Visitor 模式对 AST 的节点操作进行了抽象。首先声明了一个 Visitor 接口，所有实现了接口内 Visit 方法的类型都是访问者（技术服务人才）。Visit 方法接收一个 Node 类型的参数，Node 是 Go 中 AST 的节点，它也是一个接口，因此实际上 Node 有多种类型的实现，用于表示 AST 中各种不同的成分，这里 AST 就是我们比喻中的产业园， Node 就对应产业园中的公司。

```go {.line-numbers}
// A Visitor's Visit method is invoked for each node encountered by Walk.
// If the result visitor w is not nil, Walk visits each of the children
// of node with the visitor w, followed by a call of w.Visit(nil).
type Visitor interface {
 Visit(node Node) (w Visitor)
}
```

#### 遍历方法 Walk

ast.Walk.go 同时给出了 AST 的遍历方案：Walk 函数。Walk 函数以深度优先的方式遍历 AST，并对遍历到的节点应用 Visit 方法，当前节点的 Visit 方法会返回是否会继续遍历子节点的标志（一个 Visitor，如果是 nil 就不再继续下层遍历），Walk 根据这个标志判断是否遍历子节点，子节点处理完毕之后会对一个空节点应用 Visit 方法，即本文的主题 `w.Visit(nil)`。

```go {.line-numbers}
// Go 官方库 go/ast 包中的 Walk 实现

// Walk traverses an AST in depth-first order: It starts by calling
// v.Visit(node); node must not be nil. If the visitor w returned by
// v.Visit(node) is not nil, Walk is invoked recursively with visitor
// w for each of the non-nil children of node, followed by a call of
// w.Visit(nil).
//
func Walk(v Visitor, node Node) {
    if v = v.Visit(node); v == nil {
        return
    }

    // walk children
    // (the order of the cases matches the order
    // of the corresponding node types in ast.go)
    switch n := node.(type) {
    // Comments and fields
    case *Comment:
    // nothing to do

    // 中间有非常多的类型判断，此处省略

    case *Package:
        for _, f := range n.Files {
        Walk(v, f)
    }

    default:
        panic(fmt.Sprintf("ast.Walk: unexpected node type %T", n))
    }

    v.Visit(nil)    // 这一句就是困扰我的问题
}
```

Walk 接收两个参数，一个 Visitor，一个 Node，我们可以把 Walk 想象成产业园中的接驳车，它可以到达产业园中任何一家公司，参数 Node 就是需要服务的公司，参数 Visitor 就是提供服务的具体的技术人员，比如一个 Painter 或者一个 Hunter。

分析 Walk 的实现可以发现，虽然 Walk 中有一个 if 语句块用来判断是否进行子节点的遍历，但是 if 的判断条件是否成立却取决于访问者 Visit 方法的返回值，也就是说是否进行子节点的遍历是访问者的 Visit 方法决定的，而不是 Walk。形象的说就是接驳车会不会驶向下一个公司是车上搭载的 Visitor 决定的，Visitor 通过 Visit 方法的返回值告诉接驳车要不要继续往下走，如果继续往下走的话，接驳车最后会走到一个空的停车位（`w.Visit(nil)`），标志着这趟旅程的结束。

所以 Walk 的实现完全和数据操作逻辑进行的解耦，它只提供 AST 节点的遍历和访问功能，不关心 Node 上的具体数据操作。

#### 使用 Visitor 模式服务产业园的公司

我们以上面的比喻为例解释访问者模式的使用。定义一些基本的数据结构

```go {.line-numbers}

type Visitor interface{
    Visit(node Node) (w Visitor)
}

func Walk(v Visitor, node Node) {
    if nv := v.Visit(node); nv == nil {
        return
    }

    // walk children
    switch n := node.(type) {
        case *Company:
            for _, c := range n.Children{
                Walk(nv, c)
            }
    }
    
    v.Visit(nil)    // 返回停车场
}


// 抽象的公司
type Node interface{
    Name() string
}

// Company 绑定了 Name 方法，所以它是一种 Node
type Company struct{
    Name string
    Children []*Node
}

func (c *Company) Name() string{
    return (*c).Name
}
```

有了上面的代码我们就可以对产业园里的公司提供服务了。

##### Case 1. 遍历访问所有子节点

假如当前产业园里所有的公司都需要外墙刷新服务，那么我们可以雇佣一个 Painter 来提供服务。

```go {.line-numbers}
// 这里 Painter 实现了 Visit 方法，所以它是一个 Visitor，而且 Visit 的返回值就是 Painter 变量本身，永不为 nil，所以这里是告诉接驳车一直往下走，直到走到园区尽头（AST 的叶子节点）
type Painter int

func (p Painter) Visit(node Node) (nv Visitor){
    fmt.Printf("为 %s 提供刷墙服务\n", node.Name())
    return p
}


// 邀请一个 Painter
var p Painter = 1

// 乘坐接驳车开始服务，这里假设 root 为最开始服务的公司，且产业园结构已经存在
Walk(p, root)
```

所以接驳车 Walk 将会带着 Painter（Visitor）从 root 开始，对园区内的所有公司（Node）提供刷新外墙服务，而且没到达一个园区尽头的公司（AST 的叶子节点），接驳车都要进入空停车位一次（`w.Visit(nil)`）。

##### Case 2. 只访问当前节点

假设当前产业园有一家公司想要扩张，需要进行招聘，那么雇佣一个 Hunter 来为它服务即可，可是不能影响到其他公司。

```go {.line-numbers}
// 这里 Hunter 实现了 Visit 方法，所以它是一个 Visitor，而且 Visit 的返回值就是 nil，就是告诉接驳车我只服务这一家公司，别再往前走了，直接返回吧
type Hunter int

func (p Hunter) Visit(node Node) (nv Visitor){
    fmt.Printf("为 %s 提供招聘服务\n", node.Name())
    return nil
}


// 邀请一个 Hunter
var h Hunter = 1

// 乘坐接驳车开始服务，这里假设 company 为需要招聘服务的公司，且已经存在
// Walk 在执行时先调用 h.Visit 方法为 company 服务，之后发现 h.Visit 返回 nil，说明无需继续遍历，直接 return
Walk(h, company)
```

## 验证：LeetCode 428 分析

前文提到在遍历时处理空节点可以实现二叉树的序列化和反序列化，我们分析 N 叉树的序列化和反序列化也可以通过这种方式实现。而且 Go 的 ast 包中遍历方法 Walk 的实现本身就包含了对空节点的处理，因此我们模仿 ast.Walk 的遍历方式，使用访问者模式完成 N 叉树的序列化与反序列化。

### 题目描述
>
> [**428. 序列化和反序列化 N 叉树**](https://leetcode.cn/problems/serialize-and-deserialize-n-ary-tree/description/)
>
>序列化是指将一个数据结构转化为位序列的过程，因此可以将其存储在文件中或内存缓冲区中，以便稍后在相同或不同的计算机环境中恢复结构。
>
>设计一个序列化和反序列化 N 叉树的算法。一个 N 叉树是指每个节点都有不超过 N 个孩子节点的有根树。序列化 / 反序列化算法的算法实现没有限制。你只需要保证 N 叉树可以被序列化为一个字符串并且该字符串可以被反序列化成原树结构即可。
>
>例如，你需要序列化下面的 `3-叉` 树。
>![](https://gitlab.com/pkuhuangyong/imgbed/-/raw/main/pictures/2023/08/15_23_10_22_202308152310830.png)
>
>为 `[1 [3[5 6] 2 4]]`。你不需要以这种形式完成，你可以自己创造和实现不同的方法。
>
>或者，您可以遵循 LeetCode 的层序遍历序列化格式，其中每组孩子节点由空值分隔。
>
>
>![](https://gitlab.com/pkuhuangyong/imgbed/-/raw/main/pictures/2023/08/15_23_11_42_202308152311840.png)
>
>例如，上面的树可以序列化为
>`[1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]`
>
>你不一定要遵循以上建议的格式，有很多不同的格式，所以请发挥创造力，想出不同的方法来完成本题。
>
>**提示：**
>
>- 树中节点数目的范围是 [0, 104].
>- 0 <= Node.val <= 104
>- N 叉树的高度小于等于 1000
>- 不要使用类成员 / 全局变量 / 静态变量来存储状态。你的序列化和反序列化算法应是无状态的。

### 分析

#### 序列化

树的序列化很简单，只需深度遍历，一次将节点值输出即可，这里参考二叉树的序列化方式，在遍历时增加对空节点的处理，即前文假设的每个节点都有一个空孩子节点作为最后一个孩子节点，如下图：
![N 叉树遍历模型](https://gitlab.com/pkuhuangyong/imgbed/-/raw/main/pictures/2023/08/15_19_1_58_%E5%A4%9A%E5%8F%89%E6%A0%91%E9%81%8D%E5%8E%86%E6%A8%A1%E5%9E%8B.png)

我们要使用访问者模式来实现序列化，就需要准备相关的数据结构和方法。

首先我们需要 Visitor 的定义和遍历树结构的 Walk 方法，然后需要实现能对节点进行序列化操作的访问者，这里叫 serializer。

接着注意到题目提示：不要使用类成员 / 全局变量 / 静态变量来存储状态。而我们的 Walk 方法没有返回值，Visitor.Visit 方法的返回值仍是 Visitor，所以我们没有输出序列化结果的途径。这里考虑对 Walk 方法进行改造，使其接收一个 *[]string 类型的参数，Walk 在递归的调用时该指针会一直往下传递，Walk 返回时，我们只需读取该指针出的值即可获得序列化结果。

另外一个需要注意的点是，题目已经为我们定义好了 Node 类型，是一个结构体，考虑到 Go 中 nil 无法作为空结构体的空值，所以 Walk 方法的最后一句访问空节点需要一个实际的空节点，这里我们定义空节点如下：

```go {.line-numbers}

/**
 * Definition for a Node.
 * type Node struct {
 *     Val int
 *     Children []*Node
 * }
 */

// 代表空节点，会在 Walk 方法最后一句作为参数传入，无其他意义
var NullNode Node = Node{
    Val:      -1,
    Children: nil,
}
```

至此 Visitor 模式所需的代码如下：

```go {.line-numbers}
// 代表空节点，会在 Walk 方法最后一句作为参数传入，无其他意义
var NullNode Node = Node{
    Val:      -1,
    Children: nil,
}

// Visitor 抽象，Visit 方法中的 res 参数用来传递序列化结果
type Visitor interface {
    Visit(node Node, res *[]string) (nv Visitor)
}

func Walk(v Visitor, node Node, res *[]string) {
    nv := v.Visit(node, res)
    if nv == nil {
        return
    }

    // 递归地访问所有子节点
    for _, cn := range node.Children {
        Walk(nv, *cn, res)
    }

    // 空节点处理，表示当前节点的子节点全部访问完毕
    nv.Visit(NullNode, res)
}


// serializer 类型绑定了 Visit 方法，所以它是一个 Visitor
type serializer int

func (sv serializer) Visit(node Node, res *[]string) (nv Visitor) {
    // 序列化访问到的节点值
    *res = append(*res, strconv.Itoa(node.Val))
    // 告诉 Walk 继续访问子节点
    return sv
}
```

至此我们拥有了序列化一棵 N 叉树的所有工具，只需要初始化一个 serializer，让它去访问树的所有节点即可。
![3-叉树](https://gitlab.com/pkuhuangyong/imgbed/-/raw/main/pictures/2023/08/15_23_10_22_202308152310830.png)
以题目中第一张图为例，我们得到的序列化结果是：

```python
[1,3,5,-1,6,-1,-1,2,-1,4,-1,-1]     // -1 表示树模型中空节点的输出
```

#### 反序列化

反序列化就是将先前序列化得到的字符串重新在内存中还原出原来树的结构。序列化时，我们采用的是先序的深度优先遍历，也就是说在序列化得到的字符串中，父节点总是在其孩子节点的前面。而树的构建往往从叶节点开始逐步往上构建更加方便，所以这里考虑使用栈辅助反序列化过程。

以上图为例，序列化结果中第一个元素是树的根节点，第二个元素是其最左侧的孩子节点，如此递归，将会沿着树的最左侧路径走到最左侧叶节点并依次序列化，之后会输出 -1 表示最左侧叶节点序列化完毕。我们发现此时 -1 前面的两个节点的关系密切，前一个节点一定是后一个节点的父节点，实际上前序遍历的特点就是遍历序列中父节点之后的节点一定是其孩子节点。

有了这个特性，我们让序列化结果中的值依次入栈，每当遇到 -1，说明遍历进行到叶节点，这是可以进行父子节点的组装，栈中出栈两个元素，先出栈的是子节点，塞入后出栈的父节点的 Children 中，之后父节点入栈，然后继续对序列化结果进行上述操作，直到序列化结果中没有元素。在这个过程中要注意判断栈中元素个数是否满足连续出栈两次，如果不满足说明栈中只有一个元素了，将其出栈即是反序列化后的结果。

上述分析需要借助栈数据结构，但是 Go 中并没有现成的可用库，所以自己实现一个简单的栈，可以通过其 Push 和 Pop 方法实现入栈和出栈操作：

```go {.line-numbers}
type Stack interface {
    Push(element *Node)
    Pop() *Node
    Size() int
}

type stack struct {
    container []*Node
}

func (s *stack) Push(element *Node) {
    (*s).container = append((*s).container, element)
}

func (s *stack) Pop() *Node {
    c := (*s).container
    element := c[len(c)-1]
    (*s).container = c[:len(c)-1]
    return element
}

func (s *stack) Size() int {
     return len((*s).container)
}
```

上述分析转换成 Go 代码：

```go {.line-numbers}
func (this *Codec) deserialize(data string) *Node {
    if len(data) == 0 {
        return nil
    }

    var stk Stack = &stack{container: make([]*Node, 0,1000)}

    var root *Node

    values := strings.Split(data, ",")
    for _, va := range values {
        vi, _ := strconv.Atoi(va)
        if vi == -1 {
            if (stk).Size() == 1 {
                root = stk.Pop()
                return root
             }

            child := stk.Pop()
            parent := stk.Pop()
            parent.Children = append(parent.Children, child)
            stk.Push(parent)
        } else {
            node := Node{
                Val:      vi,
                Children: nil,
            }
            stk.Push(&node)
        }
    }
    return root
}

```

#### 验证

上述通过 Visitor 模式实现的 N 叉树序列化方法和借助栈的反序列化方法完美的通过 LeetCode 测试，耗时上击败了 100% 的 Go 用户：
![执行结果](https://gitlab.com/pkuhuangyong/imgbed/-/raw/main/pictures/2023/08/16_0_20_2_202308160020994.png)

这说明处理空节点输出的方式可以用于 N 叉树的序列化/反序列化，也就是说 ast.Walk 的实现之所以这么做可能是因为在某些地方会考虑使用 Walk 对 AST 进行序列化，并且序列化结果在必要时候会反序列化回 AST

## 结论

### 一点收获

有问题就会有答案。

这几天对这个问题的执著其实很难受，找不到答案让人抓狂。但是从结果上来看，这几天的挣扎还是有些收获的，至少获得了可能的正确方向，并且在这个方向上前进了一小步，顺便学会了一个复杂的设计模式—— Visitor 模式，还顺手 AC 了一道 LeetCode Hard 级别的题目。

### 一点遗憾

源码浩如烟海，菜鸡望洋兴叹。

虽然目前猜测 ast.Walk 的这种实现是为了 AST 的序列化/反序列化，但是目前还没有在 Go 的代码中找到证据。到底能不能找到相关证据真不好说，毕竟只是猜测。

好了，今晚就到这儿，晚安。
