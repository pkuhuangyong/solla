---
title: "About Solla"
date: 2023-08-08T16:48:05+08:00
lastmod: 2023-08-08T16:48:05+08:00
draft: false
keywords: []
description: ""
tags: ["daily"]
categories: ["随记"]
author: "Solla"

# Uncomment to pin article to front page
# weight: 1
# You can also close(false) or open(true) something for this content.
# P.S. comment can only be closed
comment: false
toc: true
autoCollapseToc: false
# You can also define another contentCopyright. e.g. contentCopyright: "This is another copyright."
contentCopyright: "知识免费 幸福无价"
reward: false
mathjax: false

# Uncomment to add to the homepage's dropdown menu; weight = order of article
# menu:
#   main:
#     parent: "docs"
#     weight: 1
---

Solla 是我的女儿，她快 6 个月啦。

有了 Solla 才知道，大抵每个男孩都想拥有自己的宝贝女儿吧。

<!--more-->

# 关于 Solla 的名字

Solla 的名字是音符名 `do、re、mi、fa、sol、la、si` 中 `sol` 和 `la` 两个音符的拼接，对应了了中国五音 `宫、商、角、徵、羽` 中的 `徵` 和 `羽`，所以 Solla 的中文名叫徵羽。

# 关于我

>
> 一个普通人，扔进人堆里最不起眼。
>
> 天坑转码，也曾为了理想付出努力。
>
> 年少轻狂时候，抬举了理想，也高看了物欲。
>
> 而立之年，开始学着过幸福的生活，好像也来得及。

2023 年 2 月的最后一周的某天，我从前公司离职。一周后，Solla 来到我的身边。

没有工作的失落和焦虑很快被宝宝到来的紧张、喜悦、手足无措一扫而空。从此开始了开心忙碌的带娃生活。

她来了，她饿了，她哭了，她慢慢的长大了，Solla 已经快 6 个月了，我也陪她成长了 6 个月。

她的纸尿裤号码从 NB 变成了 M，我的头发也经历了短 -> 长 -> 短 -> 长；

她的奶量一开始不到 20ml ，现如今喝 130ml 不用换气，我的抖音推荐也早被崔玉涛、单奶奶占领，根本刷不到游戏；

她胖了，她高了，她变结实了，她能咯咯咯地笑了……

她学会了翻身，我学会了躺平，

她开始慢慢一个一个熟悉身边的人，我开始逐渐学习看清生活的本质，

她的身边全是新鲜、刺激，好像每一天都过得不一样，我的每天都是重复放映，却因为她总能获得满足与温馨。

原来成年人真的可以从小宝宝身上看到希望。

# 联系我

网上冲浪，萍水相逢，保持距离往往会给双方好感，

但是如果本站文章有错误或对您的权益造成了侵犯，请您通过以下邮箱联系我：

<pkuhuangyong@pku.edu.cn>
