---
title: "致谢"
date: 2023-08-08T18:54:01+08:00
lastmod: 2023-08-08T18:54:01+08:00
draft: false
keywords: ["blog", "Hugo", "gitlab", "博客托管", "netlify"]
description: ""
tags: ["daily", "Hugo"]
categories: ["随记"]
author: "Solla"

# Uncomment to pin article to front page
# weight: 1
# You can also close(false) or open(true) something for this content.
# P.S. comment can only be closed
comment: false
toc: true
autoCollapseToc: false
# You can also define another contentCopyright. e.g. contentCopyright: "This is another copyright."
contentCopyright: "转载请注明出处。"
reward: false
mathjax: false

# Uncomment to add to the homepage's dropdown menu; weight = order of article
# menu:
#   main:
#     parent: "docs"
#     weight: 1
---

感谢所有本站搭建过程中所用到的开源工具的开发者。

<!--more-->

# 本站用到的建站工具

- 博客系统 [Hugo](https://gohugo.io/)
- 博客主题 [jane](https://github.com/xianmin/hugo-theme-jane)
- 编辑器 [VS Code](https://code.visualstudio.com/)
- 图床工具 [PicGo](https://picgo.github.io/PicGo-Doc/zh/)
- 代码托管 [GitLab](https://gitlab.com/)
- 版本管理 [Git](https://git-scm.com/)
- 网站托管 [Netlify](https://www.netlify.com/)
- 壁纸来源 [unsplash](https://www.unsplash.com)

# 知识无价，开源永存

请尊重和善待每一位开源作者。
